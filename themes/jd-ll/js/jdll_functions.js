$("document").ready(function(){
  var currentIndex = 0,
    items = $('.gal div'),
    itemAmt = items.length;

  function cycleItems() {
    var item = $('.gal div').eq(currentIndex);
    items.hide();
    item.css('display','inline-block');
  }
  cycleItems();

  var autoSlide = setInterval(function() {
    currentIndex += 1;
    if (currentIndex > itemAmt - 1) {
      currentIndex = 0;
    }
    cycleItems();
  }, 30000);

  $('.next').click(function() {
    clearInterval(autoSlide);
    currentIndex += 1;
    if (currentIndex > itemAmt - 1) {
      currentIndex = 0;
    }
    cycleItems();
  });

  $('.prev').click(function() {
    clearInterval(autoSlide);
    currentIndex -= 1;
    if (currentIndex < 0) {
      currentIndex = itemAmt - 1;
    }
    cycleItems();
  });

  $(".phone-nav-button").click(function(){
    this.classList.toggle("change");
    $(".phone-nav").fadeToggle();
    $(".content-padding form .search-submit").fadeToggle();
    $("body")[0].classList.toggle("lock")
  })

  $(".close").click(function(){
    $("#helloasso_popup").hide();
  });

  //programmme toggle zone
  //error !
  if($("#cal .arrow")[0]){

    $("#cal .arrow").click(function(){
      var abstract = $(this).parents().eq(1).find(".abstract");
      var row_1 = $(this).parents().eq(0);

      $(row_1[0]).toggleClass( "border" );
      $(abstract[0]).slideToggle();
      $(this).find("span").toggleClass( "rotate" );

    });

  }
  $(".info_top").click(function(){
    var abstract = $(this).parent().find(".abstract");
    $(abstract[0]).slideToggle();
  });

  $(".button").click(function(){
    var type = $(this).data("type");
    $(".button").removeClass("active");
    $(this).addClass("active");
    $(".online").hide();
    $("."+type).show();
  });

  if($(".type")[0]){
    $(".type").click(function(){
      var events = $(this).next('ul');

      $(events).slideToggle();
      var span = $(this).find("span.white");
      span.each(function(){
        $(this).toggleClass( "rotate" );
      })

    });
  }

  // very basic search bar jdll online
  var hilitor = new Hilitor("c");
  hilitor.setMatchType("open");

  $(".search_online").on("change  keydown paste input",function(){
    var v = this.value;
    var li_ar = [];
    if( v == ""){
      $("#c li").removeClass("youpi")
      li_ar = [];
      hilitor.remove();
    }else{
      hilitor.apply(v);
      $("#c mark").each(function(){
        var closest = $(this).closest("li");
        if(!li_ar.includes(closest[0])){
          li_ar.push(closest[0]);
        }
      });
    }

    if( v != ""){
      $(".online li").addClass("youpi");
      li_ar.forEach(function(item){
        $(item).removeClass("youpi");
      });
    }

  });
});
