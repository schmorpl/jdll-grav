let jdll = ()=>{

  if(document.querySelectorAll('.gal div')?.[0]){
    let currentIndex = 0;
    let items = document.querySelectorAll('.gal div');
    let itemAmt = items.length || 0;

    function cycleItems(items, currentIndex) {
      if (itemAmt == 0) return 0;
      let item = document.querySelectorAll('.gal div')[currentIndex];
      items.forEach(el => el.style.display = 'none');
      item.style.display='inline-block';
    }
    cycleItems(items, currentIndex);

    let autoSlide = setInterval(function() {
      currentIndex += 1;
      if (currentIndex > itemAmt - 1) {
        currentIndex = 0;
      }
      cycleItems(items, currentIndex);
    }, 30000);

    document.querySelector('.next').onclick = function() {
      clearInterval(autoSlide);
      currentIndex += 1;
      if (currentIndex > itemAmt - 1) {
        currentIndex = 0;
      }
      cycleItems(items, currentIndex);
    };

    document.querySelector('.prev').onclick = function() {
      clearInterval(autoSlide);
      currentIndex -= 1;
      if (currentIndex < 0) {
        currentIndex = itemAmt - 1;
      }
      cycleItems(items, currentIndex);
    };
  }

  // Handle navigation click for narrow viewports.
  document.querySelector(".phone-nav-button").onclick=function(){
    this.classList.toggle("change");
    document.querySelector(".phone-nav").classList.toggle("show");
    document.querySelector(".content-padding form .search-submit").classList.toggle("show");
    document.querySelector(".bck").classList.toggle("lock")
  }

  //programmme toggle zone
  //error !

  if(document.querySelectorAll("#cal .arrow")?.[0]){

    document.querySelector("#cal .arrow").onclick = function(){
      let abstract = document.querySelector(this).parents()[1].querySelector(".abstract");
      let row_1 = document.querySelector(this).parents()[0];

      document.querySelector(row_1[0]).classList.toggle( "border" );
      document.querySelector(abstract[0]).slideToggle();
      document.querySelector(this).querySelector("span").classList.toggle( "rotate" );
    };

  }
  if (document.querySelector(".info_top")){
    document.querySelector(".info_top").onclick = function(){
      let abstract = document.querySelector(this).parents().querySelector(".abstract");
      document.querySelector(abstract[0]).slideToggle();
    };
  }

  if (document.querySelector(".button")){
    document.querySelector(".button").onclick = function(){
      let type = document.querySelector(this).data("type");
      document.querySelector(".button").classList.remove("active");
      document.querySelector(this).classList.add("active");
      document.querySelector(".online").style.display = 'none';
      document.querySelector("."+type).style.display = 'block';
    };
  }

  if(document.querySelector(".type")?.[0]){
    document.querySelector(".type").onclick = function(){
      let events = document.querySelector(this).next('ul');

      document.querySelector(events).slideToggle();
      let span = document.querySelector(this).querySelector("span.white");
      span.forEach((el)=>{
        el.classList.toggle( "rotate" );
      })
    };
  }
};
jdll();
